Author :
  * Pascal VILAREM pvi-gitlab-contact /chez/ vilarem /point/ net

Contributors:

  * Michel-Marie MAUDET mmaudet /chez/ linagora /point/ com

    I did not expect my boss to be my first contributor but he was the first to download docsign, betatest it, find problems, and help to analyse them. More, he contributed the first version of TROUBLESHOOT.md file which is also the first external contribution to docsign. So a big thanks to him !
