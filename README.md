# docsign

Docsign is yet another PDF doc signing tool with a few nice features.

Origin & influences
===================

The idea is born during the "Corona virus" worldwide crisis. 

My mother needed to reply to an important letter and I could only help her remotely due to lock down.
She had to sign several PDF documents received by email and push them by french "electronic recommended letter".

At the same moment several friends had similar problems to sign PDF documents. And i discovered later that more people than I thought had to struggle with that problem including people in my company.

I searched for a nice (and free and opensource) tool which could do everything i wanted using only the basic tools we can find on linux : ls, mv, ln, find, grep, sed... plus usual pdf manipulation tools : pdftk and pdfjam suites.

I found a few things (lots of java, python...), and among them i have to mention : falsisign from Edouard Klein: https://gitlab.com/edouardklein/falsisign which inspired me.


So i decided to write a new shell script built like a tiny application with options and translations and management functions to add several signatures allowing to randomly choose among them for each new signing.

I added also a concept of "setup" allowing to define different behaviors for each page : signing only the first, signing only the 2 last, signing all but the last...

I also wanted it to be able to take a scanned file of a signature and make it transparent before storing it for further use... that's what it does currently.

My friends will be able to use this with defaults setups. For business needs more complex setup files will probably be needed.

Hope this tiny tool will help people.

Bests from pvi on gitlab


Status
======

At the moment Docsign is in early beta (and feedback would be more than welcome).


Installation
============

Just git clone the repository or get an archive and unarchive it for example in $HOME/Applications/docsign

Then get inside and run ./docsign.sh

It will check all the dependancies and complain if one is missing.

Then you will be able to apt install or yum install or urpmi or dnf install the needed packages.

Then run it again till it says it's ready to be used


Basic use
=========

Adding a signature
------------------

To add my_png_scanned_signature.png file just run this :
```sh
docsign.sh add signature my_png_scanned_signature.png
```

Docsign will take the input file, optimize it, remove the background, color the signature pixels and convert it as a composable PDF.

(So you do not need to provide a transparent signature file).

**IMPORTANT** : The resolution of the signature input file is supposed to be roughly **650x300 px**

For my tests I used a png image with a white background cropped out of a scanner generated pdf file.

If you provide a really different file you will have to play on scale factor and maype adjust positions to get a good result.


Signing a document
------------------

To sign my_administrative_pass.pdf just run this :
```sh
docsign.sh sign my_administrative_pass.pdf
```
you will get a new file named my_administrative_pass-signed.pdf


Advanced use
============

Adding a signature
------------------

You can precise if you want to set a color for your signature :
```sh
docsign.sh add signature --color "blue" my_png_scanned_signature.png
```

You can use shortened parameters if you're lazy:
```sh
docsign.sh a s my_png_scanned_signature.png
```

You can precise if you want to set a color to your signature :
```sh
docsign.sh a s -c "blue" my_png_scanned_signature.png
```

Signing a document
------------------

If you want to use an other setup than the default you ccan use other setups or create your own.

Standard setups are :
  * default or all : signing everything as A4 portrait in the down right corner.
  * first : signing only the first page as A4 portrait in the down right corner.
  * last : signing only the last page as A4 portrait in the down right corner.

Examples :
```sh
docsign.sh sign --setup "first" my_administrative_pass.pdf
```
you will get a new file named my_administrative_pass-signed.pdf with only the signature on the first page

```sh
docsign.sh sign --setup "last" my_administrative_pass.pdf
```
you will get a new file named my_administrative_pass-signed.pdf with only the signature on the last page

You can also use shortened parameters :

```sh
docsign.sh s -s "last" my_administrative_pass.pdf
```


Resetting config
----------------

If you want to clean all the config and restart from 0 :

```sh
docsign.sh resetconfig
```
Beware : it erases previously stored signatures and setups... It's really a restart from zero.


Changelog
=========

2020-04-12 : Added Import/export for setups

2020-04-12 : First rough version

2020-04-11 : Creation


Roadmap / Ideas
===============

Next features
-------------
The next features I'm planning to add:
  * create easily new setups <-- I'm searching for ideas to make that easy for people
  * delete signatures and setups <-- Should be easy

Possible syntax for setups :

  docsign add setup mccav default:sign:0 p1:sign:1 p1:orientation:portrait p1:scale:0.3 p1:horiz:2cm p1:verti:-12cm

  docsign edit setup mccav p12:clean <-- to remove all params for p12 ?

  docsign edit setup mccav r1:sign:1 r1:orientation:landscape r1:horiz:7 r1:verti:-3 r1:scale:-0.4 <-- to set signature on for the last-1 page with landscape orientation and bigger scale.

Ideas
-----
And others features I'm considering:
  * amend/edit existing setups ? <-- Same need for ideas / create feature
  * naming signatures with labels ?
  * forcing the use of a specific signature instead of randomly picking one ?
  * adding digital signing with public/private keys ?
  * add a concept of "persona" to allow random selection of signature files but within the same group. For people sharing the same computer account or people needding at least 2 kind of signatures (personal life and professional life...)
  

END OF FILE
