#!/bin/bash

# LICENSE : TODO

# define constants
declare -r TRUE=0
declare -r FALSE=1
declare -r YES=1
declare -r NO=0

declare -r TOOLS_LIST="mkdir rm cp mv ln realpath find sed grep sort tr dirname basename cat wc printf pwd shuf less ls file seq convert pdfseparate pdfjoin pdfjam pdftk"
declare -r CONFIG_DIR="${HOME}/.config/docsign"
declare -r BIN_CONFIG_FILE="${CONFIG_DIR}/config/binaries"

# define core functions
function findBinary ()
{
  binary=$1
  for basePath in /bin /sbin /usr/bin /usr/local/bin
  do
    binaryPath="${basePath}/${binary}"
    if [ -x "${binaryPath}" ]
    then
      echo "${binaryPath}"
      break 1
    fi
  done
}

function checkBinary ()
{
  binary=$1
  spaceClean=""
  if [ "x${WC_BIN}" != "x" ]
  then
    spaceCount=$(( 15 - $(echo $binary | ${WC_BIN} -c) ))
    for ((i=0; i<=$spaceCount; i++)); do spaceClean=" ${spaceClean}"; done
  fi
  
  echo -n "Checking ${binary}..."
  binaryPath="$(findBinary ${binary})"
  if [ "x" != "x${binaryPath}" ]
  then
    echo -e "${spaceClean}\e[1m\e[32mOK\e[0m: ${binaryPath}"
    return $TRUE
  else
    echo -e "${spaceClean}\e[1m\e[91mMISSING\e[0m: \e[1m\e[91m${binary}\e[0m please install it"
    return $FALSE
  fi
}

function exitFunction ()
{
  exitCode="$1"
  exitMessage="$2"
  echo ""
  echo -e "${exitMessage}"
  exit $exitCode
}

# Check init
function checkInit ()
{
  checkResult=$TRUE
  # Needed for checkBinary
  WC_BIN=$(findBinary wc)
  
  if [ ! -f ${CONFIG_DIR}/config/binaries ]
  then
    # TODO : Translate
    echo "Docsign Init. Let me initialize a few things please..."
    for binary in ${TOOLS_LIST}
    do
      checkBinary ${binary}
      [ $? -eq $FALSE ] && checkResult=$FALSE
    done
    # TODO : Translate
    [ $checkResult -eq $FALSE ] && exitFunction 1 "Some needed binaries were not found. Please install what's missing"
    
    # Needed for the rest of checkInit process
    MKDIR_BIN=$(findBinary mkdir)
    RM_BIN=$(findBinary rm)
    TR_BIN=$(findBinary tr)
    CP_BIN=$(findBinary cp)
    DIRNAME_BIN=$(findBinary dirname)
    PWD_BIN=$(findBinary pwd)
    LN_BIN=$(findBinary ln)
    GREP_BIN=$(findBinary grep)
    SED_BIN=$(findBinary sed)
    REALPATH_BIN=$(findBinary realpath)
    CONVERT_BIN=$(findBinary convert)
    PRINTF_BIN=$(findBinary printf)
    
    # Checking Policy of ImageMagick for blocking rule
    IM_CONFIG_DIR=$(${CONVERT_BIN} -list configure | ${SED_BIN} -ne "s/^CONFIGURE_PATH *//;s/\/$//p")
    
    alertPolicyCount=$( ${GREP_BIN} '^ *<policy domain="coder" *rights="none" *pattern=".*PDF.*"' "${IM_CONFIG_DIR}/policy.xml" | ${WC_BIN} -l )
    
    esc=$(${PRINTF_BIN} '\033')
    if [ $alertPolicyCount -gt 0 ]
    then
      # TODO : Translate
      echo ""
      echo -e "\e[1m\e[91mALERT\e[0m in ${IM_CONFIG_DIR}/policy.xml: \e[1m\e[91mplease check that PDF writing is not blocked\e[0m"
      echo "The concerned lines are:"
      echo -ne "\e[34m"
      ${GREP_BIN} "^ *<policy domain=\"coder\" *rights=\"none\" *pattern=\".*PDF.*\"" "${IM_CONFIG_DIR}/policy.xml" | ${SED_BIN} -e "s/PDF/${esc}[1m${esc}[91mPDF${esc}[34m/;s/none/${esc}[1m${esc}[91mnone${esc}[34m/"
      echo -ne "\e[0m"
      echo ""
    fi
      
    
    # Initializing config : config dir
    ${MKDIR_BIN} -p ${CONFIG_DIR}/{config,signatures,setups}

    # Initializing config : APP_DIR
    APP_DIR="$(${DIRNAME_BIN} $(${REALPATH_BIN} "$0"))"
    export APP_DIR
    
    # Initializing config : default setups
    ${CP_BIN} -f ${APP_DIR}/setups/*.dsf ${CONFIG_DIR}/setups/
    
    # Initializing binaries path
    ${RM_BIN} -f ${BIN_CONFIG_FILE}
    
    echo "APP_DIR=\"${APP_DIR}\"" >> ${BIN_CONFIG_FILE}
    echo "export APP_DIR"  >> ${BIN_CONFIG_FILE}
    
    for binary in ${TOOLS_LIST}
    do
      binaryVarName="$(echo ${binary} | ${TR_BIN} 'a-z' 'A-Z')_BIN" 
      binaryPath="$(findBinary ${binary})"
      echo "$binaryVarName=\"${binaryPath}\"" >> ${BIN_CONFIG_FILE}
    done
    echo ""
    
    # Sortcuts
    docsignCommand="$0"
    if [ -d ${HOME}/bin ]
    then
      if [ ! -h "${HOME}/bin/docsign" ] && [ ! -e "${HOME}/bin/docsign" ]
      then
        ${LN_BIN} -s ${APP_DIR}/docsign.sh ${HOME}/bin/docsign
        docsignCommand="docsign"
        # TODO : Translate
        echo -e "\e[1m\e[32mCOOL\e[0m: You can now use \e[1m\e[32mdocsign\e[0m command provided ${HOME}/bin is in your path"
      else
        # TODO : Translate
        echo -e "\e[1m\e[31mWARNING\e[0m: the command docsign already exists in ${HOME}/bin"
      fi
      if [ ! -h ${HOME}/bin/ds ] && [ ! -e ${HOME}/bin/ds ]
      then
        ${LN_BIN} -s ${APP_DIR}/docsign.sh ${HOME}/bin/ds
        # TODO : Translate
        echo -e "\e[1m\e[32mCOOL\e[0m: You can now use \e[1m\e[32mds\e[0m command provided ${HOME}/bin is in your path"
      else
        # TODO : Translate
        echo -e "\e[1m\e[31mWARNING\e[0m: The command ds already exists in ${HOME}/bin and points to an other program"
      fi
    fi

    exitFunction 0 "\e[1m\e[32mDocsign has been initialized. You can now run it with help argument to know more\e[0m:\n${docsignCommand} help"
  fi

  return $TRUE
}

#
# Main
#

#
# Init
#
checkInit

source ${BIN_CONFIG_FILE}

source ${APP_DIR}/lib/functions

#
# Parameters
#
[[ $# -eq 0 ]] && showHelp 0

while [[ $# -gt 0 ]]
do
  key="${1}"

  case ${key} in
  s|sign)
    source ${APP_DIR}/lib/sign
    shift # next argument
    doSign $*
    exit 0
    ;;
  a|add)
    source ${APP_DIR}/lib/setup
    shift # past argument
    doAdd $*
    exit 0
    ;;
  l|list)
    source ${APP_DIR}/lib/setup
    shift # past argument
    doList $*
    exit 0
    ;;
  i|import)
    source ${APP_DIR}/lib/setup
    shift # past argument
    doImport $*
    exit 0
    ;;
  e|export)
    source ${APP_DIR}/lib/setup
    shift # past argument
    doExport $*
    exit 0
    ;;
  d|del|delete)
    source ${APP_DIR}/lib/setup
    shift # past argument
    doDelete $*
    exit 0
    ;;
  h|help)
    showHelp 0
    exit 0
    ;;
  qh|quickhelp)
    showQuickHelp
    exit 0
    ;;
  sl|showlicense)
    showLicense
    exit 0
    ;;
  resetconfig)
    shift # past argument
    doResetConfig
    exit 0
    ;;
  *)
    showHelp 1 wrong_argument ${key}
  ;;
  esac
done

exit 0

